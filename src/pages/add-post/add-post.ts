import { FriendService } from '../../app/services/friend.service';
import { HomePage } from '../home/home';
import { PostService } from '../../app/services/post.service';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AddPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-post',
  templateUrl: 'add-post.html',
})
export class AddPostPage implements OnInit {
  public friends: Friend[];
  public post = {};
  public result: any;  

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private postService: PostService,
    private friendService: FriendService) {
  }

  ngOnInit(){
    this.friendService.getAll().subscribe(data => this.friends = data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPostPage');
  }

  async onSubmit(){
    this.postService.add(this.post).subscribe(data => this.result = data);
    await this.navCtrl.push(HomePage);
  }

}

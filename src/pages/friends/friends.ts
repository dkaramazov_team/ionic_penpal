import { FriendService } from '../../app/services/friend.service';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FriendsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage implements OnInit {
  friends: Friend[];
  friend_list: Friend[];

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
    private friendService: FriendService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FriendsPage');
  }

  ngOnInit(){
    this.friendService.getAll().subscribe(data => this.friends = data);
  }

}

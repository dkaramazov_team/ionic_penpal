import { AddPostPage } from '../add-post/add-post';
import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { FriendService } from '../../app/services/friend.service';
import { PostService } from '../../app/services/post.service';

import { PostDetailPage } from '../post-detail/post-detail';
import { FriendsPage } from '../friends/friends';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  posts: Post[];
  friends: Friend[];

  constructor(public navCtrl: NavController, private postService: PostService, private friendService: FriendService) {
  }

  ngOnInit(){
    this.postService.getAll().subscribe(data => this.posts = data);
    this.friendService.getAll().subscribe(data => this.friends = data);
  }

   ionViewWillEnter(){
     this.postService.getAll().subscribe(data => this.posts = data);
    }

   postSelected(event, post){
     this.postService.increment(post, 'views').subscribe(data => post = data);
     this.navCtrl.push(PostDetailPage, {
       post: post
     });
   }

   addPost(){
     this.navCtrl.push(AddPostPage);
   }

   selectFriends(){
     this.navCtrl.push(FriendsPage);
   }

}

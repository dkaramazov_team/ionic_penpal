import { HomePage } from '../home/home';
import { PostService } from '../../app/services/post.service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PostDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post-detail',
  templateUrl: 'post-detail.html',
})
export class PostDetailPage {
  public post: Post;
  public result: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private postService: PostService){
    this.post = navParams.get('post');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PostDetailPage');
  }

  async delete(id){
    this.postService.delete(id).subscribe(data => this.result = data);
    await this.navCtrl.push(HomePage);
  }

  increment(post: Post, prop: string){
    this.postService.increment(post, prop).subscribe(data => this.result = data);
  }

}

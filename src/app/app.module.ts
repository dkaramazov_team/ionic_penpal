import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpModule } from '@angular/http';

import { FriendsPage } from '../pages/friends/friends';
import { AddPostPage } from '../pages/add-post/add-post';
import { PostDetailPage } from '../pages/post-detail/post-detail';
import { HomePage } from '../pages/home/home';

import { MyApp } from './app.component';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AddPostPage,
    PostDetailPage,
    FriendsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AddPostPage,
    PostDetailPage,
    FriendsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

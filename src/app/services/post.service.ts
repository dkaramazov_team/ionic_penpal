import {Injectable} from '@angular/core';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PostService{
    http: any;
    apiKey: string;
    postsUrl: string;
    private headers: Headers = new Headers();

    constructor(http: Http){
        this.http = http;
        this.apiKey = 'vVH4pKyzeY67OG2oEjjsZW0uTmFYvzNR';
        this.postsUrl = 'https://api.mlab.com/api/1/databases/ionic_penpal/collections/posts';
        //this.postsUrl = 'https://mongodb://penpal_mobile:realpassword@ds011790.mlab.com:11790/ionic_penpal';

        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Access-Control-Allow-Origin', `*`);
        this.headers.append('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
        this.headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');
    }

    getAll(){
        return this.http.get(`${this.postsUrl}?apiKey=${this.apiKey}`, {headers: this.headers})
            .map(res => res.json());
    }

    add(post: any){
        post.date = Date.now();
        post.views = 0;
        post.hearts = 0;
        return this.http.post(`${this.postsUrl}?apiKey=${this.apiKey}`, JSON.stringify(post), {headers: this.headers})
            .map(res => res.json());
    }

    delete(id){
        return this.http.delete(`${this.postsUrl}/${id}?apiKey=${this.apiKey}`, JSON.stringify(id), {headers: this.headers})
            .map(res => res.json());
    }

    increment(post, prop){
        post[prop] += 1;
        let obj = {};
        obj[prop] = post[prop];
        return this.http.put(`${this.postsUrl}/${post._id.$oid}?apiKey=${this.apiKey}`,
            JSON.stringify({ "$set" : obj }),
            {headers: this.headers})
            .map(res => res.json());
    }

}
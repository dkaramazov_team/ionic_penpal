import {Injectable} from '@angular/core';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class FriendService{
    http: any;
    apiKey: string;
    friendsUrl: string;
    private headers: Headers = new Headers();

    constructor(http: Http){
        this.http = http;
        this.apiKey = 'vVH4pKyzeY67OG2oEjjsZW0uTmFYvzNR';
        this.friendsUrl = 'https://api.mlab.com/api/1/databases/ionic_penpal/collections/friends';
        //this.friendsUrl = 'https://mongodb://penpal_mobile:realpassword@ds011790.mlab.com:11790/ionic_penpal';

        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Access-Control-Allow-Origin', `*`);
        this.headers.append('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
        this.headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token');
    }

    getAll(){
        return this.http.get(`${this.friendsUrl}?apiKey=${this.apiKey}`, {headers: this.headers})
            .map(res => res.json());
    }

    add(post){
        return this.http.post(`${this.friendsUrl}?apiKey=${this.apiKey}`, JSON.stringify(post), {headers: this.headers})
            .map(res => res.json());
    }

    delete(id){
        return this.http.delete(`${this.friendsUrl}/${id}?apiKey=${this.apiKey}`, JSON.stringify(id), {headers: this.headers})
            .map(res => res.json());
    }
}
interface Post{
    friend: Friend;
    date: number;
    title: string;
    body: string;
    image: string;
    hearts: number;
    views: number;
}